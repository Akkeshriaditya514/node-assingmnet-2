const path = require("path");

const express = require("express");

const adminController = require("../controllers/admin");

const router = express.Router();

router.post("/create-company", adminController.getCreatecompany);

router.get("/get-companies", adminController.getcompanies);

router.put("/update-company/:companyId", adminController.getUpdatecompany);

router.delete("/delete-company/:companyId", adminController.postDeletecompany);

module.exports = router;
